export function u_getValueA(article){
 return Math.max(article.timeLimitArticle, article.averageDayUse, article.timeFactor, article.lotSize);
}
export function u_getValueB(article){
    return article.timeLimitArticle * article.averageDayUse;
}
export function u_getValueC(article){
    return Math.round(article.timeLimitArticle * article.averageDayUse * Math.round((article.timeFactor / 100 )*100)/100  * (article.variabilityFactor / 100)); 
}
export function u_getValueD(article){
    return article.timeLimitArticle * article.averageDayUse * Math.round((article.timeFactor / 100 )*100)/100 ;
}
export function u_getValueE(article){
   return article.averageDayUse !== 0 ? Math.round((u_getValueA(article) / article.averageDayUse )*100)/100 : 0 ;
}
export function u_getValueF(article){
  return article.averageDayUse !== 0 ? Math.round((u_getValueB(article) / article.averageDayUse)*100)/100 : 0;
}
export function u_getValueG(article){
    return article.averageDayUse !== 0 ?  Math.round((u_getValueC(article) / article.averageDayUse)*100)/100 : 0;
}
export function u_getValueH(article){
    return article.averageDayUse !== 0 ?  Math.round((u_getValueD(article) / article.averageDayUse)*100)/100 : 0;
}
export function u_getValueI(article){
    return Math.round(u_getValueA(article) + u_getValueB(article) + u_getValueC(article) + u_getValueD(article));
}
export function u_getValueJ(article){
    return Math.round(u_getValueB(article) + u_getValueC(article) + u_getValueD(article));
}
export function u_getValueK(article){
    return Math.round(u_getValueC(article) + u_getValueD(article));
}
export function u_getValueL(article){
   return Math.round(u_getValueK(article) +  Math.round((u_getValueA(article) / 2)*100)/100);
}
export function u_getValueM(article){
    return u_getValueL(article) * article.articlePrice;
}
export function u_getValueN(article){
    return article.averageDayUse !== 0 ? Math.round(Math.round(u_getValueA(article) / article.averageDayUse)*100)/100 : 0;
}
export function u_getValueO(article){
    return u_getValueA(article);
}
export function u_getValueP(article, firstArticle){
    return firstArticle == null ? null : u_getValueL(article) - u_getValueL(firstArticle);
}
export function u_getValueQ(article, firstArticle){
    return  u_getValueM(article) -  u_getValueM(firstArticle);
}
export function u_getValueR(article, firstArticle){
    return  u_getValueN(article) - u_getValueN(firstArticle);
}
export function u_getValueS(article, firstArticle){
    return  u_getValueO(article) -  u_getValueO(firstArticle);
}