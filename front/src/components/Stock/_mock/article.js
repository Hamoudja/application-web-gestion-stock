import articles from '../_mock/article.json';
import {setTimeout} from 'core-js';

const fetch = (mockData, time = 0) => {
return new Promise(function (resolve) {
    return setTimeout(function(){
        return resolve(mockData);
    }, time)
});
}

export default{
   getArticles(){
    let articleList = [];
    return fetch(articles, 100).then(result => {
        result.articles.forEach(article => {
            let articleId = 'article' + article.id
            let articleStorage = localStorage.getItem(articleId);
            if(articleStorage == null){
                localStorage.setItem(articleId, JSON.stringify(article));
                articleList.push(article);
            }
            else{
                article =  JSON.parse(localStorage.getItem(articleId));
                articleList.push(article);
            }
        });
        return articleList;
    });
   }
}