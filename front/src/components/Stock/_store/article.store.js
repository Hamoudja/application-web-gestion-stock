import Vue from 'vue';
import Vuex from 'vuex';
import Client from '../_mock/article.js';
Vue.use(Vuex);
export default new Vuex.Store({

    state: {
         articleList: [],
    },
    mutations: {
        SET_CORE_DATA(state, newValue){
             state.articleList = newValue;
        },
    },
    
    actions: {
        getListArticles({commit}){
                return Client.getArticles().then(result =>{
                    commit("SET_CORE_DATA", result);
                    return true;
                }).catch(e => {console.error(e);})
        },
        setTimeLimitArticle({state}, params){
            
         let article = state.articleList.find(a => a.id == params.id);
         article.timeLimitArticle = params.value;
         let articleId = 'article' + params.id
         localStorage.removeItem(articleId);
         localStorage.setItem(articleId, JSON.stringify(article));
        },
        setLotSize({state}, params){
            let article = state.articleList.find(a => a.id == params.id);
            article.lotSize = params.value;
            let articleId = 'article' + params.id
            localStorage.removeItem(articleId);
            localStorage.setItem(articleId, JSON.stringify(article));
            
        },
        setAverageDayUse({state}, params){
            let article = state.articleList.find(a => a.id == params.id);
            article.averageDayUse = params.value;
            let articleId = 'article' + params.id
            localStorage.removeItem(articleId);
            localStorage.setItem(articleId, JSON.stringify(article));
        },
        setArticlePrice({state}, params){
            let article = state.articleList.find(a => a.id == params.id);
            article.articlePrice = params.value;
            let articleId = 'article' + params.id
            localStorage.removeItem(articleId);
            localStorage.setItem(articleId, JSON.stringify(article));
        },
        setTimeFactor({state}, params){
            let article = state.articleList.find(a => a.id == params.id);
            article.timeFactor = params.value;
            let articleId = 'article' + params.id
            localStorage.removeItem(articleId);
            localStorage.setItem(articleId, JSON.stringify(article));
        },
        setVariabilityFactor({state}, params){
            let article = state.articleList.find(a => a.id == params.id);
            article.variabilityFactor = params.value;
            let articleId = 'article' + params.id
            localStorage.removeItem(articleId);
            localStorage.setItem(articleId, JSON.stringify(article));
        } 
    }
    });
    